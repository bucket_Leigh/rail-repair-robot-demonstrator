'''
Code for Gerards pi on the railrepair robot

code gets arduino bytes within the loop. If the bytes change then a  
message is sent to the display pi using the raspberry pi's bluetooth. '''

import serial


def bluetooth_message_send( input_as_string , read_serial):
    '''work in progress- 
    uses the pi's bluetooth module to send a 
    message to a second pi (the handheld display device)'''
    return()
    

def difference_check(message_history, input_as_string, read_serial):
    '''checks that the serial imput has changed.
    Uses the bluetooth_message_send function (which needs finishing before the final project deliverable'''    

    if message_history != input_as_string :
    # 'message_history' is a parameter that is persistent  
    # while input_as_string is getting constantly changed/updated
        bluetooth_message_send( input_as_string , read_serial )
        #print('test message')
        # use the bluetooth message function to shoot a message to the display
    message_history = input_as_string[0] 
    return(message_history)


#ser = serial.Serial('/dev/ttyUSB0',9600)
'''might need to un-comment above line'''
input_as_string = [0]
message_history = []

while True:
    #following lines should be added to Gerard's main-loop
    ser = serial.Serial('/dev/ttyUSB0',9600) #might be very wrong
    read_serial=ser.readline()
    input_as_string[0] = str(int (ser.readline(),16) )
    message_history= difference_check(message_history, input_as_string, read_serial)
