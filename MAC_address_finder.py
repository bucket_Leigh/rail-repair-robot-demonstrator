# -*- coding: utf-8 -*-
"""
Created on Fri Dec  7 11:30:42 2018

@author: e801129
copied from: https://www.raspberrypi-spy.co.uk/2012/06/finding-the-mac-address-of-a-raspberry-pi/

Code prints the MAC-address of the Pi it's run on
"""

def getMAC(interface='eth0'):
  # Return the MAC address of the specified interface
  try:
    str = open('/sys/class/net/%s/address' %interface).read()
  except:
    str = "00:00:00:00:00:00"
  MAC_address = str[0:17]
  return ( MAC_address )


MAC_address = getMAC()
print (MAC_address)  

