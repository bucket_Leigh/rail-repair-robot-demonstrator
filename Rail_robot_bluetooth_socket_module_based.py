# -*- coding: utf-8 -*-
"""
Created on Fri Jan 11 15:28:34 2019

@author: e801129

largely cribbed from : 
http://blog.kevindoran.co/bluetooth-programming-with-python-3/

The sending device runs socketClient.py, and the receiving device runs socketServer.py. 

A simple Python script to send messages to a sever over Bluetooth using
Python sockets (with Python 3.3 or above).
"""


def socketClient( message ):
    '''Code to go onto the information sending side of the Bluetooth connection.
    i.e. Goes on Gerards pi.
    Uses the sockets module'''
    import socket
    
    interface='eth0'
    str = open('/sys/class/net/%s/address' %interface).read()
    serverMACAddress = str[0:17]
    # A MAC-address is a unique identifying number for bluetooth kit.
    
    port = 4
    s = socket.socket(socket.AF_BLUETOOTH, 
                      socket.SOCK_STREAM, 
                      socket.BTPROTO_RFCOMM)
    s.connect((serverMACAddress,port))
    
    s.send(bytes(message, 'UTF-8'))
    s.close()
    return


def socketServer():
    '''Code to go onto the information recieving side of the Bluetooth connection.
    Uses the sockets module'''
    import socket
    
    interface='eth0'
    str = open('/sys/class/net/%s/address' %interface).read()
    hostMACAddress = str[0:17]  # The MAC address of the handset-Pi
    # A MAC-address is a unique identifying number for bluetooth kit.
    
    port = 4 # An arbitrary choice. However, it must match the port used by the client.
    size = 1024
    s = socket.socket(socket.AF_BLUETOOTH, 
                      socket.SOCK_STREAM, 
                      socket.BTPROTO_RFCOMM)
    s.bind((hostMACAddress,port))
    s.listen(1)
    client, address = s.accept()
    
    data = client.recv(size)
    client.send(data)
    
    return (data)



def break_contact_with_hardware():
    #function that closes the socket being used for bluetooth connection
    import socket
    
    s = socket.socket(socket.AF_BLUETOOTH, 
                      socket.SOCK_STREAM, 
                      socket.BTPROTO_RFCOMM)
    client, address = s.accept()
    client.close()
    s.close()
    return



def Bluetooth_reconnection():
    #Function that closes and reopens the bluetooth on the handset
    #not used currently, but could be used from a GUI button in the situation that the link is not working.
    import socket
    
    break_contact_with_hardware()
    
    interface='eth0'
    str = open('/sys/class/net/%s/address' %interface).read()
    hostMACAddress = str[0:17]  # The MAC address of the handset-Pi    
    
    s = socket.socket(socket.AF_BLUETOOTH, 
                      socket.SOCK_STREAM, 
                      socket.BTPROTO_RFCOMM)
    port = 4
    s.bind((hostMACAddress,port))

    return
