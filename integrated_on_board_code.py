# -*- coding: utf-8 -*-
"""
Created on Fri Dec  7 12:52:31 2018

@author: e801129 & Gerard

Code integration- Gerards code with my code for 
Serial connections for the pi to the Arduino.

Unmitigated Genius!
"""

import serial
import time
from selenium import webdriver


def bluetooth_message_send( input_as_string , read_serial):
    '''work in progress- 
    uses the pi's bluetooth module to send a 
    message to a second pi (the handheld display device)'''
    return()
    

def difference_check(message_history, input_as_string, read_serial):
    '''checks that the serial imput has changed.
    Uses the bluetooth_message_send function (which needs finishing before the final project deliverable'''    

    if message_history != input_as_string :
    # 'message_history' is a parameter that is persistent  
    # while input_as_string is getting constantly changed/updated
        bluetooth_message_send( input_as_string , read_serial )
        #print('test message')
        # use the bluetooth message function to shoot a message to the display
    message_history = input_as_string[0] 
    return(message_history)


def left_rail_print():
    '''prints on left rail'''
    time.sleep(5) # Let the user actually see something!
    #element5=driver.find_element_by_css_selector("button[ng-click='closeTemp();RSPrinter.setExtruderTemperature(active.state.activeExtruder,t.temp)']")
    element5=driver.find_elements_by_css_selector("button[ng-click='printGCode(f)']")
    element5[0].click()
    # clicks first "Print G-code"
    
    time.sleep(5)
    element5=driver.find_element_by_css_selector("button[ng-click='confirmYes()']")
    element5.click()
    time.sleep(5)
    return()


def right_rail_print():
    '''prints on right rail'''
    time.sleep(5) # Let the user actually see something!
    element5=driver.find_elements_by_css_selector("button[ng-click='printGCode(f)']")
    element5[1].click()
    
    time.sleep(5)
    element5=driver.find_element_by_css_selector("button[ng-click='confirmYes()']")
    element5.click()
    time.sleep(5)
    return()


input_as_string = [0]
message_history = []
driver = webdriver.Chrome()  # Optional argument, if not specified will search path.
driver.get('http://localhost:3344/#!/printer/Dobot/print');


while True:
    #following lines should be added to Gerard's main-loop
    ser = serial.Serial('/dev/ttyUSB0',9600) #might be very wrong
    read_serial=ser.readline()
    input_as_string[0] = str(int (ser.readline(),16) )
    message_history= difference_check(message_history, input_as_string, read_serial)
    # End of my code within loop (LK)


    #G-man's stuff....
    if input_as_string[0]=='1': 
        #Checks if the print instruction has been recieved
        left_rail_print()
        
        
    if input_as_string[0]=='2':
        #checks for message to print the other rail
        right_rail_print()
    pass  
    #end of G's awesome code
   
