# -*- coding: utf-8 -*-
"""
Created on Fri Aug 24 10:06:11 2018

@author: e801129-- L.Kirkwood


Code developed for GUI work
Screen resolution, 800 X 460. Building a grid of 20x20 boxes (40boxes x 13 boxes)
UPDATE- OK, so resolution options on the Pi are limited-- 720x480. Changed.
"""

import time
import datetime
import tkinter
#from tkinter import messagebox
import numpy as np

#message='' #used later in appended_message class method

def message_sequence():
    '''creates a mock message, as if from the onboard pi, as will be delivered by Bluetooth.
    The file format delivered is hexidecimal
    This function is only used for the testing of the GUI and is not 
    intended to be part of the projects final delivered code
    '''
    import random
    a = random.randint(0, 13 ) 
    # creates random integer
    message_from_Pi = hex(a)  
    # converts to a string that displays the information in a hex format
    
    print('mock input from arduino & Onboard Pi' , message_from_Pi )
    return (message_from_Pi)

def socketServer():
    '''Code to go onto the information recieving side of the Bluetooth connection.
    Uses the sockets module'''
    import socket
    
    interface='eth0'
    str = open('/sys/class/net/%s/address' %interface).read()
    hostMACAddress = str[0:17]  # The MAC address of the handset-Pi
    # A MAC-address is a unique identifying number for bluetooth kit.
    
    port = 4 # An arbitrary choice. However, it must match the port used by the client.
    size = 1024
    s = socket.socket(socket.AF_BLUETOOTH, 
                      socket.SOCK_STREAM, 
                      socket.BTPROTO_RFCOMM)
    s.bind((hostMACAddress,port))
    s.listen(1)
    client, address = s.accept()
    
    data = client.recv(size)
    client.send(data)
    print('data type of returned value: socketServer', type(data) )
    return (data)


def message_checking():
    '''    Code to check the hardware:
    If the messages are coming through from the train Pi we get updates 
    (in bytes coding) which will need to be translated into information.
    Intention is that this function is within the tKinter mainloop and 
    that the "message" object is passed back to the GUI'''
    message_from_Pi = socketServer()
    # Checks with the Bluetooth 
    '''
    message_from_Pi = message_sequence()
    # can use above line for GUI testing- if Bluetooth is not working
    '''
    message_from_Pi = int(message_from_Pi , 16) # conversion of hex-string to int.
    
    if message_from_Pi == 0:
        message = '\n Push start to initiate sequence'
    elif message_from_Pi == 1:
        message = ' \n Moving back to origin'        
    elif message_from_Pi == 2:
        message = ' \n Origin reached: push start to initiate fault search and repair'    
    elif message_from_Pi == 3:
        message = ' \n Starting fault search'    
    elif message_from_Pi == 4:
        message = ' \n Fault found on left rail'    
    elif message_from_Pi == 5:
        message = ' \n Fault found on right rail'    
    elif message_from_Pi == 6:
        message = ' \n Moving back to fault location'    
    elif message_from_Pi == 7:
        message = ' \n Fault location reached'    
    elif message_from_Pi == 8:
        message = ' \n Sending commands to robot arm'    
    elif message_from_Pi == 9:
        message = ' \n Repair in progress'    
    elif message_from_Pi == 10:
        message = ' \n Repair complete: continuing search'
    elif message_from_Pi == 11:
        message = ' \n End reached: moving back to origin'  
    elif message_from_Pi == 12:
        message = ' \n Returning to origin'  
    elif message_from_Pi == 13:
        message = ' \n Origin reached: push start to look for faults'
    else:
        message = '\n No message from hardware: possible connection issue'
        #above checks various conditions and modifies the "message" depending on input
        #The else statement catches any weird issues
    return(message)     

    
class MessageRecord():
    '''records the appended messages, makes them useable outside usual scope'''
    def __init__(self ):
        self.appended_message = ('')
        
    
    def update_appended_message(self, message):
        '''class method used to update the appended_message numpy array
        Returns 'trimmed_messages' which is displayed on the GUI '''
        appended_message = self.appended_message                
        if appended_message == '':
            #fresh start- first-loop
            appended_message =  message
            message_string   = appended_message 
        else:
            previous_messages = appended_message.splitlines(keepends=True)
            if previous_messages[-1] != message :
                #checks the message is unique
                appended_message = (appended_message + message)
                #Above appends 'message' onto the existing 'appended_message' array
            elif appended_message[-1] == message:
                # same message recieved
                pass
                #we leave the message_string as the previous appended message.
        if ( appended_message.count('\n') ) >5 :
            line_count = appended_message.count('\n')
            # line_count is used to hve a number of the lines of messages
            message_list= appended_message.splitlines(keepends=True)
            # Returns list of the lines in the string, breaking at line boundaries.
            del message_list[ 0:((line_count-1)-5)]
            # deletes old messages
            message_string = ( message_list[0] + message_list[1] + 
            message_list[2] + message_list[3] + message_list[4] )
            # message_string is the messages all back in string form 
            # and at correct length for GUI printing
        else:
            # appended_message is less than 5 lines, not much to do
             message_string   = appended_message 
        self.appended_message = appended_message 
        #saves appended_message into the class attribute 
        return( message_string )    
    
    
class Application(tkinter.Frame ):
    def __init__(self, master=None):
        super().__init__(master)
        self.grid()
        
        self.create_widgets()
        
#    def GUI_end(event=None):
#        
#        if tkinter.messagebox.askokcancel('Confirm system shutdown?', 
#         'OK to shutdown system? \n (Cancel to return to GUI)', parent=root ):
#            #using parent=root should overlay the message onto root window. *should*
#            root.destroy() 

    def create_widgets(self ): #, appended_message):
        '''creates tkinter widgets'''
    
        label_colour = 'LightGoldenrod1'
        box_bg_colour = 'light goldenrod yellow' 
        root.configure(background = 'pale goldenrod')
        x_value =0
        y_value =0
        border_value = 0

        '''buttons codes'''                                   
        self.quit = tkinter.Button( text="GUI shutdown ", bg='LightGoldenrod2' , 
                                   fg="red", command= root.destroy ).grid(
                                   row=9, column=22, columnspan=4, rowspan=4 ,
                                   sticky='w'+'e'+'n'+'s', 
                                   padx=x_value, pady=y_value) 
        '''labels '''
        logo_photo1 = tkinter.PhotoImage(file='Network-Rail.gif')

        Logo = tkinter.Label( image=logo_photo1)
        Logo.config(bg='white', fg='blue', 
                            relief=tkinter.RIDGE,width=20, height=20)#, width=standard_width)        
        Logo.image = logo_photo1 
        Logo.grid(row=0,column=1, columnspan=18, rowspan=8 , 
                          sticky='w'+'e'+'n'+'s', padx=x_value, pady=y_value) 
       
       
        logo_photo2 = tkinter.PhotoImage(file='Marques-CranfieldUniversity.gif')

        Logo = tkinter.Label( image=logo_photo2)
        Logo.config(bg='white', fg='blue', 
                            relief=tkinter.RIDGE,width=20, height=20)#, width=standard_width)        
        Logo.image = logo_photo2 
        Logo.grid(row=0,column=20, columnspan=8, rowspan=8 , 
                          sticky='w'+'e'+'n'+'s', padx=x_value, pady=y_value)       
       
       
        logo_photo3 = tkinter.PhotoImage(file='in2smart2.gif')
        Logo = tkinter.Label( image=logo_photo3)
        Logo.config(bg='white', fg='blue', 
                            relief=tkinter.RIDGE,height=5)#, width=standard_width)        
        Logo.image = logo_photo3 
        Logo.grid(row=15,column=1, columnspan=6, rowspan=8 , 
                          sticky='w'+'e'+'n'+'s', padx=x_value, pady=y_value)     
       
       
        logo_photo4 = tkinter.PhotoImage(file= 'S2R_LOGO_HR_Copy.gif')
        Logo = Logo = tkinter.Label( image=logo_photo4)
        Logo.config(bg='white', fg='blue', 
                            relief=tkinter.RIDGE,height=5)#, width=standard_width)        
        Logo.image = logo_photo4 
        Logo.grid(row=15,column=8, columnspan=8, rowspan=8 , 
                          sticky='w'+'e'+'n'+'s', padx=x_value, pady=y_value)
              
              
        logo_photo5 = tkinter.PhotoImage(file='H2020_300ppp2.gif')  
        #logo_photo4 = tkinter.PhotoImage(file='Network-Rail-Rainbow.gif')

        Logo = tkinter.Label( image=logo_photo5)
        Logo.config(bg='white', fg='blue', 
                            relief=tkinter.RIDGE,height=5)#, width=standard_width)        
        Logo.image = logo_photo5 
        Logo.grid(row=15,column=17, columnspan=16, rowspan=8 , 
                          sticky='w'+'e'+'n'+'s', padx=x_value, pady=y_value)        
        
                          
        ''' information boxes'''
        message = message_checking()
        print('debugging find me text #1', message)
        systemMessage = tkinter.Label(text='System messages')
        systemMessage.config(bg=label_colour, 
                             relief=tkinter.RIDGE)#,width=standard_width)
        systemMessage.grid(row=8, column=1, columnspan=18, 
                           sticky='w'+'e'+'n'+'s', padx=x_value, pady=y_value )
        
        
        trimmed_messages =  z.update_appended_message(  message )
        
        statusMessage = tkinter.Label(text=trimmed_messages)
        statusMessage.config(bg=box_bg_colour, bd=border_value, 
                             relief=tkinter.SUNKEN)
        statusMessage.grid(row=9, column=1, columnspan=18 , rowspan = 5, 
                           sticky='w'+'e'+'n'+'s', padx=x_value, pady=y_value)
   
   
        '''frame title & geometry & grid sizing'''
        root.title("Sol Bot process control")
        geometry = "800x480"
        root.geometry(geometry)
        col_count, row_count = root.grid_size()
        for col in range(col_count):
            root.grid_columnconfigure(col, minsize=20)
        
        for row in range(row_count):
            root.grid_rowconfigure(row, minsize=20)
        
        root.overrideredirect(1) 
        #hides the frame of the GUI window
        
        print('loop') #TODO: remove in final version
        #print('trimmed_messages', trimmed_messages )
        b = self.create_widgets
        
        root.after(3000, b )
        return


        
z = MessageRecord() 
root = tkinter.Tk() 
app = Application( master=root )
app.mainloop()
# sets up the mainloop