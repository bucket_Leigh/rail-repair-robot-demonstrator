# -*- coding: utf-8 -*-
"""
Created on Fri Aug 24 10:06:11 2018

@author: e801129-- L.Kirkwood
"""



'''
Code developed for GUI work
Screen resolution, 800 X 460. Building a grid of 20x20 boxes (40boxes x 13 boxes)
UPDATE- OK, so resolution options on the Pi are limited-- 720x480. Changed.
UPDATE- added a geometry variable that's used in multiple places- 
        done so that changes can be quickly made.
'''


import time
import datetime
import tkinter
import numpy as np



def message_checking():
    '''
    Code to check the hardware:
    If the messages are coming through from the train Pi we get updates 
    (in bytes coding) which will need to be translated into information.
    Intention is that this function is within the tKinter mainloop and 
    that the "message" object is passed back to the '''
    
    message_from_Pi='' # = result of bluetooth poll #TODO: needs finalising
        # Checks with the Bluetooth the 
    try:
        message_from_Pi =  int.from_bytes(message_from_Pi, byteorder='big')  
        #TODO: test byteorder='litte' option, as i have little idea how this function works
        #Convert from byte format into string. TODO: Test on  ubuntu
        
        message_from_Pi = str( message_from_Pi )
        #final conversion to string
            
        message = ''
        #defines a string to later assign to.
        if message_from_Pi == '0':
            message = 'Push start to initiate sequence'
        if message_from_Pi == '1':
            message = 'Moving back to origin'        
        if message_from_Pi == '2':
            message = 'Origin reached, push start to initiate fault search and repair'    
        if message_from_Pi == '3':
            message = 'Starting fault search'    
        if message_from_Pi == '4':
            message = 'Fault found on left rail'    
        if message_from_Pi == '5':
            message = 'Fault found on right rail'    
        if message_from_Pi == '6':
            message = 'Moving back to fault location'    
        if message_from_Pi == '7':
            message = 'Fault location reached'    
        if message_from_Pi == '8':
            message = 'Sending commands to robot arm'    
        if message_from_Pi == '9':
            message = 'Repair in progress'    
        if message_from_Pi == '10':
            message = 'Repair complete: continuing search'
        if message_from_Pi == '11':
            message = 'End reached, moving back to origin'  
        if message_from_Pi == '12':
            message = 'Returning to origin'  
        if message_from_Pi == '13':
            message = 'Origin reached, push start to look for faults'
            

    except TypeError:        
        message = 'No message from hardware: possible connection issue'
        #No message recieved scenario
    return(message)     


class Application(tkinter.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.grid()

        self.create_widgets(appended_message)

        
    def create_widgets(self , appended_message):

        label_colour = 'LightGoldenrod1'
        box_bg_colour = 'light goldenrod yellow' 
        root.configure(background = 'pale goldenrod')
        x_value =0#2
        y_value =0#2
        border_value = 0#3

        '''buttons codes'''

        self.quit = tkinter.Button( text="GUI shutdown ", bg='LightGoldenrod2' , 
                                   fg="red", command=root.destroy ).grid(
                                   row=8, column=20, columnspan=4, rowspan=4 ,
                                   sticky='w'+'e'+'n'+'s', 
                                   padx=x_value, pady=y_value)   
        '''labels '''
        logo_photo1 = tkinter.PhotoImage(file='Network-Rail.gif')

        Logo = tkinter.Label( image=logo_photo1)
        Logo.config(bg='white', fg='blue', 
                            relief=tkinter.RIDGE,width=20, height=20)#, width=standard_width)        
        Logo.image = logo_photo1 
        Logo.grid(row=0,column=0, columnspan=18, rowspan=8 , 
                          sticky='w'+'e'+'n'+'s', padx=x_value, pady=y_value) 
       
       
        logo_photo2 = tkinter.PhotoImage(file='Marques-CranfieldUniversity.gif')

        Logo = tkinter.Label( image=logo_photo2)
        Logo.config(bg='white', fg='blue', 
                            relief=tkinter.RIDGE,width=20, height=20)#, width=standard_width)        
        Logo.image = logo_photo2 
        Logo.grid(row=0,column=19, columnspan=6, rowspan=8 , 
                          sticky='w'+'e'+'n'+'s', padx=x_value, pady=y_value)       
       
       
        logo_photo3 = tkinter.PhotoImage(file='in2smart2.gif')

        Logo = tkinter.Label( image=logo_photo3)
        Logo.config(bg='white', fg='blue', 
                            relief=tkinter.RIDGE,height=5)#, width=standard_width)        
        Logo.image = logo_photo3 
        Logo.grid(row=15,column=0, columnspan=8, rowspan=8 , 
                          sticky='w'+'e'+'n'+'s', padx=x_value, pady=y_value)     
       
       
        logo_photo4 = tkinter.PhotoImage(file='H2020_300ppp.gif')  
        #logo_photo4 = tkinter.PhotoImage(file='Network-Rail-Rainbow.gif')

        Logo = tkinter.Label( image=logo_photo4)
        Logo.config(bg='white', fg='blue', 
                            relief=tkinter.RIDGE,height=5)#, width=standard_width)        
        Logo.image = logo_photo4 
        Logo.grid(row=15,column=13, columnspan=12, rowspan=8 , 
                          sticky='w'+'e'+'n'+'s', padx=x_value, pady=y_value)        
       
       
        ''' information boxes'''
        systemMessage = tkinter.Label(text='System messages')
        systemMessage.config(bg=label_colour, 
                             relief=tkinter.RIDGE)#,width=standard_width)
        systemMessage.grid(row=8, column=0, columnspan=18, 
                           sticky='w'+'e'+'n'+'s', padx=x_value, pady=y_value )
        message = message_checking()
        try:
            appended_message = np.append(appended_message, message)
        except TypeError:
            appended_message = np.array(message)
        #Above tries to append 'message' onto the existing 'appended_message' array
        if (len(appended_message)) >5:
            trimmed_messages = appended_message[len(appended_message)-5:len(appended_message)]
            #trim list to last entries
        else:
            trimmed_messages = appended_message
        trimmed_messages = np.array2string(trimmed_messages)
        #message         = 'status message stand-in'
        statusMessage = tkinter.Label(text=trimmed_messages)
        statusMessage.config(bg=box_bg_colour, bd=border_value, 
                             relief=tkinter.SUNKEN)
        statusMessage.grid(row=9, column=0, columnspan=18 , rowspan = 4, 
                           sticky='w'+'e'+'n'+'s', padx=x_value, pady=y_value)
   
        '''frame title & geometry & grid sizing'''
        root.title("Sol Bot process control")
        geometry = "800x480"
        root.geometry(geometry)
        col_count, row_count = root.grid_size()
        for col in range(col_count):
            root.grid_columnconfigure(col, minsize=20)
        
        for row in range(row_count):
            root.grid_rowconfigure(row, minsize=20)
        #root.grid_columnconfigure(0, minsize=20)
        
        #root.after(200, create_widgets ) #TODO: change cycle time for final version
        #message = message_checking()
        root.overrideredirect(1) #hides the frame of the GUI window
        print('loop') #TODO: remove in final version
        print('size of grid', root.size()  )
        print('screen resolution = ' , geometry )
     

print('tkinter version: ', tkinter.TkVersion )
appended_message = []

root = tkinter.Tk() #Need this for the geometry change


app = Application(master=root)

app.mainloop()